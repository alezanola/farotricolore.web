# Faro Tricolore association

"Associazione Culturale Faro Tricolore" is an italian non-profit assiciation whose goal is to divulge Risorgimento history through public events, books presentations and much more.

Visit the [website](https://farotricolore.it/) to learn more!

## Web app

This project is the website web app published at the previous link written using Angular2 Framework. 

It is automatically published by GitLab CI pipeline to the Cloud Run container service of the Google Cloud Platform.
It is being published to a container management platform because the project implements the SSR (Server Side Rendering) to optimize SEO.
