import {Component, Injector} from '@angular/core';
import {NgcCookieConsentService} from 'ngx-cookieconsent';
import {Platform} from '@angular/cdk/platform';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private injector: Injector,
              private platform: Platform) {
    if(platform.isBrowser){
      injector.get(NgcCookieConsentService);
    }
  }
}
