import {BrowserModule, Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { PagesModule } from './pages/pages.module';

import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { FooterComponent } from './footer/footer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgcCookieConsentConfig, NgcCookieConsentModule} from 'ngx-cookieconsent';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'farotricolore.it'
  },
  palette: {
    popup: {
      background: '#000',
      text: '#ffffff',
      link: '#ffffff'
    },
    button: {
      background: '#0081A7',
      text: '#ffffff',
      border: 'transparent'
    }
  },
  position: 'bottom',
  theme: 'classic',
  type: 'info',
  content: {
    message: 'Questo sito usa i cookies per migliorare l\'esperienza di navigazione. Continuando su questo sito accetto il loro utilizzo.',
    dismiss: 'Ok!',
    deny: 'Rifiuta',
    link: 'Maggiori informazioni',
    href: 'cookie',
    policy: 'Cookie Policy'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    FooterComponent
  ],
  imports: [
    NgcCookieConsentModule.forRoot(cookieConfig),
    NgbModule,
    PagesModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    FormsModule,
    SharedModule,
    BrowserAnimationsModule
  ],
  providers: [Title],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
