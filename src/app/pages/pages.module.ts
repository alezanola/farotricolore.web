import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { ContactsComponent } from './shared/components/contacts/contacts.component';
import { ChiSiamoComponent } from './chi-siamo/chi-siamo.component';
import { HomeEventComponent } from './shared/components/home-event/home-event.component';

import { SharedModule } from '../shared/shared.module';
import { AccountService } from './shared/services/account.service';
import { SharedComponentsModule } from './shared/components/shared-components.module';
import {EventsService} from './shared/services/events.service';
import {HttpService} from './shared/services/http.service';
import {RouterModule} from '@angular/router';
import {NgbCarouselModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { HomeNextEventModalComponent } from './shared/components/home-next-event-modal/home-next-event-modal.component';
import {FormsModule} from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';
import {NgcCookieConsentModule} from 'ngx-cookieconsent';

@NgModule({
  declarations: [
    HomeComponent,
    ContactsComponent,
    ChiSiamoComponent,
    HomeEventComponent,
    HomeNextEventModalComponent,
    NotFoundComponent
  ],
  imports: [
    NgcCookieConsentModule,
    CommonModule,
    SharedModule,
    SharedComponentsModule,
    RouterModule,
    NgbCarouselModule,
    NgbModalModule,
    FormsModule
  ],
  providers: [
    EventsService,
    AccountService,
    HttpService
  ],
  exports: [
    HomeComponent
  ]
})
export class PagesModule { }
