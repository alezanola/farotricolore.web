import { Component, OnInit } from '@angular/core';

import { EventLite } from '../shared/models/event-lite';
import { Router } from '@angular/router';
import {CommonService} from '../shared/services/common.service';
import {EventsService} from '../shared/services/events.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loading: boolean;

  events: EventLite[];

  constructor(private eventsService: EventsService,
              private router: Router,
              public commonService: CommonService,
              private titleService: Title,
              private metaService: Meta) {
    this.loading = true;
    this.setTitle();
    this.configureMeta();
   }

  ngOnInit() {
    this.eventsService.getFutureEvents(3, 0).subscribe((events) => {
      this.events = events;
      this.loading = false;
    });
  }

  setTitle(): void {
    this.titleService.setTitle('Faro Tricolore');
  }

  configureMeta(): void {
    this.metaService.addTags([
      {
        name: 'og:description',
        content: "L'associazione culturale \"Faro Tricolore\" è nata a Desenzano del Garda il 22 gennaio 2010 da un " +
          "gruppo di amici guidato dalla volontà di preservare e tramandare, in funzione della coesione nazionale, i valori fondanti della " +
          "nostra nazione: l'Italia."
      },
      {
        name: 'og:image',
        content: '/assets/images/logos/LogoFaroB.png'
      },
      { name: 'robots', content: 'index, follow' }
    ]);
  }
}
