import { Component} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-chi-siamo',
  templateUrl: './chi-siamo.component.html',
  styleUrls: ['./chi-siamo.component.scss']
})
export class ChiSiamoComponent {

  constructor(private titleService: Title,
              private metaService: Meta) {
    this.setTitle();
    this.configureMeta();
  }

  setTitle(): void {
    this.titleService.setTitle('Chi siamo - Faro Tricolore');
  }

  configureMeta(): void {
    this.metaService.addTags([
      {
        name: 'og:description',
        content: "L'associazione culturale \"Faro Tricolore\" è nata a Desenzano del Garda il 22 gennaio 2010 da un " +
          "gruppo di amici guidato dalla volontà di preservare e tramandare, in funzione della coesione nazionale, i valori fondanti della " +
          "nostra nazione: l'Italia."
      },
      {
        name: 'og:image',
        content: '/assets/images/logos/LogoFaroB.png'
      },
      { name: 'robots', content: 'index, follow' }
    ]);
  }

}
