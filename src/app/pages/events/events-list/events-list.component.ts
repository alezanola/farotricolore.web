import { Component, OnInit } from '@angular/core';
import {EventsService} from '../../shared/services/events.service';
import {CommonService} from '../../shared/services/common.service';
import {Meta, Title} from '@angular/platform-browser';
import {EventLite} from '../../shared/models/event-lite';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {

  public events: EventLite[];
  public loading: boolean;
  public loadingMoreEvents: boolean;
  public qty: number;
  public page: number;

  constructor(private eventsService: EventsService,
              public commonService: CommonService,
              private titleService: Title,
              private metaService: Meta) {
    this.setTitle();
    this.configureMeta();
  }

  ngOnInit() {
    this.loading = true;
    this.loadingMoreEvents = false;
    this.qty = 8;
    this.page = 0;
    this.loadEvents();
  }

  private loadEvents() {
    this.eventsService.getPastEvents(this.qty, this.page).subscribe(events => {
      this.events = events;
      this.loading = false;
    });
  }

  loadMoreEvents() {
    this.page++;
    this.loadingMoreEvents = true;
    this.eventsService.getPastEvents(this.qty, this.page).subscribe(events => {
      this.events = this.events.concat(events);
      this.loadingMoreEvents = false;
    });
  }

  setTitle(): void {
    this.titleService.setTitle('Eventi - Faro Tricolore');
  }

  configureMeta(): void {
    this.metaService.addTags([
      {
        name: 'description',
        content: "Gli eventi che abbiamo organizzato"
      },
      {
        name: 'og:image',
        content: '/assets/images/logos/LogoFaroB.png'
      },
      { name: 'robots', content: 'index, follow' }
    ]);
  }

}
