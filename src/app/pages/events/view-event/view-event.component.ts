import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {EventsService} from '../../shared/services/events.service';
import {EventDetailed} from '../../shared/models/event-detailed';
import {CommonService} from '../../shared/services/common.service';
import {Location} from '@angular/common';
import {AccountService} from '../../shared/services/account.service';

@Component({
  selector: 'app-event-edit',
  templateUrl: './view-event.component.html',
  styleUrls: ['./view-event.component.scss']
})
export class ViewEventComponent implements OnInit {

  public loading: boolean;
  public imageLoading: boolean;
  public event: EventDetailed;

  constructor(private route: ActivatedRoute,
              private eventsService: EventsService,
              public commonService: CommonService,
              public location: Location,
              public usersService: AccountService) {
    this.loading = true;
    this.imageLoading = true;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(param => {
      const id = param.get('id');
      this.loadEvent(id);
    });
  }

  private loadEvent(id: string) {
    this.eventsService.getEvent(id).subscribe(event => {
      this.event = event;
      this.loading = false;
    });
  }
}
