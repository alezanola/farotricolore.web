import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventsRoutingModule } from './events-routing.module';
import { EventsListComponent } from './events-list/events-list.component';
import { ViewEventComponent } from './view-event/view-event.component';
import { FormsModule } from '@angular/forms';
import {SharedComponentsModule} from '../shared/components/shared-components.module';
import {NgcCookieConsentModule} from 'ngx-cookieconsent';

@NgModule({
  declarations: [
    EventsListComponent,
    ViewEventComponent
  ],
  imports: [
    CommonModule,
    EventsRoutingModule,
    FormsModule,
    SharedComponentsModule,
    NgcCookieConsentModule
  ]
})
export class EventsModule { }
