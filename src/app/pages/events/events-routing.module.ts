import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsListComponent } from './events-list/events-list.component';
import { ViewEventComponent } from './view-event/view-event.component';

const routes: Routes = [
  { path: '', component: EventsListComponent },
  { path: ':id', component: ViewEventComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
