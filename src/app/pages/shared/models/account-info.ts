export class AccountInfo {
  email: string;
  name: string;
  surname: string;
  role: string;
  token: string;
  tokenExpiration: Date;
}
