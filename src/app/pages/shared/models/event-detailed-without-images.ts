import {Image} from './image';

export class EventDetailedWithoutImages {
  id: string;
  name: string;
  shortDescription: string;
  longDescription: string;
  startDate: Date;
  endDate: Date;
  location: string;
  poster: Image;
  hideEvent: boolean;
}
