import {Image} from './image';
import {Video} from './video';

export class EventMedia {
  id: string;
  name: string;
  images: Image[];
  videos: Video[];
}
