import {Image} from './image';

export class EventWithFirstImage {
  id: string;
  name: string;
  location: string;
  startDate: Date;
  firstImage: Image;
}
