import {Image} from './image';

export class EventLite {
  id: string;
  name: string;
  shortDescription: string;
  location: string;
  startDate: Date;
  endDate: Date;
  poster: Image;
  hideEvent: boolean;
}
