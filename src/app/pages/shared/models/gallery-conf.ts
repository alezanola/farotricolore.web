import {GALLERY_CONF} from 'ngx-image-gallery';

export class GalleryConf implements GALLERY_CONF {
  backdropColor?: string;
  closeOnEsc: boolean;
  imageBorderRadius: string;
  imageOffset: string;
  imagePointer: boolean;
  inline: boolean;
  reactToKeyboard: boolean;
  reactToMouseWheel: boolean;
  reactToRightClick: boolean;
  showArrows: boolean;
  showCloseControl: boolean;
  showDeleteControl: boolean;
  showExtUrlControl: boolean;
  showImageTitle: boolean;
  showThumbnails: boolean;
  thumbnailSize: number;
}
