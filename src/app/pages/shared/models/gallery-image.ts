import {GALLERY_IMAGE} from 'ngx-image-gallery';

export class GalleryImage implements GALLERY_IMAGE {
  url: string;
  // _cached: boolean;
  // altText: string;
  // extUrl: string;
  // extUrlTarget: string;
  // thumbnailUrl: string;
  // title: string;
}
