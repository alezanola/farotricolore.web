import {Image} from './image';

export class EventDetailed {
  id: string;
  name: string;
  shortDescription: string;
  longDescription: string;
  startDate: Date;
  endDate: Date;
  location: string;
  poster: Image;
  mediaExist: boolean;
  hideEvent: boolean;
}
