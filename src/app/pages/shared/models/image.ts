export class Image {
  id: string;
  small: string;
  medium: string;
  large: string;
}
