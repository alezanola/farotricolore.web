import {AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl, SafeScript} from '@angular/platform-browser';

@Component({
  selector: 'app-youtube-video',
  templateUrl: './youtube-video.component.html',
  styleUrls: ['./youtube-video.component.scss']
})
export class YoutubeVideoComponent {
  url: SafeResourceUrl;

  @Input()
  set videoCode(videoCode: string) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoCode);
  }

  constructor(private sanitizer: DomSanitizer) { }

}
