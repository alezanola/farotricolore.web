import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-event',
  templateUrl: './home-event.component.html',
  styleUrls: ['./home-event.component.scss']
})
export class HomeEventComponent implements OnInit {
  @Input() id: string;
  @Input() name: string;
  @Input() date: string;
  @Input() location: string;

  constructor() { }

  ngOnInit() {
  }

}
