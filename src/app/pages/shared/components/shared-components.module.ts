import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import { YoutubeVideoComponent } from './youtube-video/youtube-video.component';

@NgModule({
  declarations: [ LoadingComponent, YoutubeVideoComponent ],
  imports: [
    CommonModule,
    NgbCarouselModule
  ],
  exports: [
    LoadingComponent,
    YoutubeVideoComponent
  ]
})
export class SharedComponentsModule { }
