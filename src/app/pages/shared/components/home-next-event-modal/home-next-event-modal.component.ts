import {AfterContentInit, AfterViewChecked, AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {EventLite} from '../../models/event-lite';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {CommonService} from '../../services/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-next-event-modal',
  templateUrl: './home-next-event-modal.component.html',
  styleUrls: ['./home-next-event-modal.component.scss']
})
export class HomeNextEventModalComponent implements OnInit, AfterViewChecked {
  @Input() event: EventLite;
  @ViewChild('modalTemplate', { static: false }) modal: ElementRef;
  private opened: boolean;

  constructor(private config: NgbModalConfig,
              private modalService: NgbModal,
              public commonService: CommonService,
              public router: Router) {}


  ngOnInit() {
    this.config.size = this.event.poster ? 'xl' : 'lg';
    this.config.scrollable = true;
    this.opened = false;
  }

  ngAfterViewChecked(): void {
    if (!this.opened && this.event && this.modal) {
      this.modalService.open(this.modal);
      this.opened = true;
    }
  }

  openExternalLink(link: string) {
    window.open(link);
  }
}
