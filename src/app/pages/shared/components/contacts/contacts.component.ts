import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  readonly contacts: { role: string, name: string, email: string }[] = [
    { role: 'Presidente', name: "Maria D'Arconte", email: 'info@farotricolore.it' },
    { role: 'Vicepresidente', name: 'Marco Barziza', email: 'marcobarziza@gmail.com' },
    { role: 'Coordinatore Faro Tricolore Giovani', name: 'Marco Locatelli', email: 'locatelli.marc@tiscali.it' },
    { role: 'Pagina Facebook', name: 'Guido Zancarli', email: 'guido.zancarli@email.it' },
    { role: 'Pagina Facebook', name: 'Annalisa Ronchi', email: 'lysaron.ar@gmail.com' },
    { role: 'Sito web', name: 'Alessandro Zanola', email: 'alessandro.zanola@farotricolore.it' },
    { role: 'Grafica sito web', name: 'Fabian Grassi', email: 'fabian.grassi@farotricolore.it' },
    { role: 'Grafica locandine', name: 'Marialuisa Roversi', email: 'marialuisa.roversi@gmail.com' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
