import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {AppConfig} from '../../../shared/config/app-config';
import {Observable} from 'rxjs';
import {EventMedia} from '../models/event-media';
import {EventWithFirstImage} from '../models/event-with-first-image';
import {Video} from '../models/video';

@Injectable({
  providedIn: 'root'
})
export class EventMediaService {

  private readonly url: string;

  constructor(private httpService: HttpService,
              private config: AppConfig) {
    this.url = config.baseV1Url + 'events/media/';
  }

  public getPastEventsImage(qty: number, page: number): Observable<EventWithFirstImage[]> {
    return this.httpService.get(`${this.url}${qty}/${page}`);
  }

  public getEventMedia(eventId: string): Observable<EventMedia> {
    return this.httpService.get<EventMedia>(this.url + eventId);
  }

  public insertImages(eventId: string, images: string[]): Observable<any> {
    return this.httpService.post(this.url + eventId + "/images", images);
  }

  public editVideos(eventId: string, videos: Video[]): Observable<any> {
    return this.httpService.post(this.url + eventId + "/videos", videos);
  }

  public deleteImage(eventId: string, imageId: string) {
    return this.httpService.delete(this.url + eventId + '/' + imageId);
  }

}
