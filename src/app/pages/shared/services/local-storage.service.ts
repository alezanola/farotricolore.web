import { Injectable } from '@angular/core';
import { AccountInfo } from '../models/account-info';
import {EventDetailedWithoutImages} from '../models/event-detailed-without-images';
import {EventLite} from '../models/event-lite';
import {EventDetailed} from '../models/event-detailed';
import {EventWithFirstImage} from '../models/event-with-first-image';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  private get<T>(key: string): T {
    return JSON.parse(localStorage.getItem(key));
  }

  private set(key: string, obj) {
    localStorage.setItem(key, JSON.stringify(obj));
  }

  private remove(key: string) {
    localStorage.removeItem(key);
  }



  public getAccountInfo(): AccountInfo {
     return this.get('accountInfo');
  }
  public setAccountInfo(accountInfo: AccountInfo): void {
    this.set('accountInfo', accountInfo);
  }
  public removeAccountInfo(): void {
    this.remove('accountInfo');
  }

  /*
  public savePastEvents(events: EventDetailedWithoutImages[]) {
    this.set('pastEvents', events);
  }
  public getPastEvents(): EventDetailedWithoutImages[] {
    return this.get('pastEvents');
  }


  public saveFutureEvents(events: EventLite[]) {
    this.set('futureEvents', events);
  }
  public getFutureEvents(): EventLite[] {
    const events = this.get<EventLite[]>('futureEvents');
    if (events.length === 0) { return []; }
    while (events.length === 0 || events[0].startDate < new Date()) {
      events.shift();
    }
    return events;
  }


  public savePastEventsImage(events: EventWithFirstImage[]) {
    this.set('pastEventsImage', events);
  }
  public getPastEventsImage(): EventWithFirstImage[] {
    return this.get('pastEventsImage');
  }


  public saveEvent(event: EventDetailed) {
    this.set(event.id, event);
    const eventsSaved = this.get<string[]>('eventsSaved');
    if (eventsSaved && !eventsSaved.find(val => val === event.id)) {
      eventsSaved.push(event.id);
      this.set('eventsSaved', eventsSaved);
      console.log('Added to events saved');
    }
  }
  public getEvent(id: string): EventDetailed {
    return this.get(id);
  }
  */
}
