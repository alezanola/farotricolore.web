import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/shared/config/app-config';
import { LocalStorageService } from 'src/app/pages/shared/services/local-storage.service';
import { AccountInfo } from 'src/app/pages/shared/models/account-info';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private url: string;
  private loginStateObservable: Subject<boolean>;

  constructor(private http: HttpClient,
              private config: AppConfig,
              private localStorageService: LocalStorageService,
              private router: Router
              ) {
    this.url = config.baseV1Url + 'user/';

    this.loginStateObservable = new Subject();
  }

  public authenticate(credentials): Observable<AccountInfo> {
    const observable = this.http.post<AccountInfo>(this.url + 'login/',
                   credentials);
    observable.subscribe(result => {
      this.localStorageService.setAccountInfo(result);
      this.loginStateObservable.next(true);
    });

    return observable;
  }

  public logout() {
    this.localStorageService.removeAccountInfo();
    this.loginStateObservable.next(false);
    this.router.navigate(['/admin/login']);
  }

  public isLoggedIn(): boolean {
    const accountInfo = this.localStorageService.getAccountInfo();
    return accountInfo && accountInfo.token && new Date(accountInfo.tokenExpiration).getDate() < Date.now();
  }

  public subscribeLoginState(callback) {
    this.loginStateObservable.subscribe(callback);
  }
}
