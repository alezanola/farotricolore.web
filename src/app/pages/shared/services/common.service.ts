import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  public getFormattedDate(startDate: Date) {
    startDate = new Date(startDate);

    const options = { weekday: 'long', month: 'long', day: '2-digit', year: 'numeric' };
    let stringDate = startDate.toLocaleString('it-IT', options);
    stringDate = stringDate.substr(0, 1).toUpperCase() + stringDate.substr(1);
    return stringDate;
  }

  public getSlashFormattedDate(date: Date) {
    date = new Date(date);
    return date.toLocaleDateString('it-IT');
  }
}
