import { Injectable } from '@angular/core';
import { EventLite } from 'src/app/pages/shared/models/event-lite';
import { Observable } from 'rxjs';
import { EventModel } from '../../admin/shared/models/event-model';
import { AppConfig } from 'src/app/shared/config/app-config';
import {IdModel} from '../models/id-model';
import {HttpService} from './http.service';
import {EventDetailed} from '../models/event-detailed';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private readonly url: string;

  constructor(private httpService: HttpService,
              private config: AppConfig) {
    this.url = config.baseV1Url + 'events/';
  }

  public getFutureEvents(qty: number, page: number): Observable<EventLite[]> {
    return this.httpService.get<EventLite[]>(`${this.url}${qty}/${page}`);
  }

  public getPastEvents(qty: number, page: number): Observable<EventLite[]> {
    return this.httpService.get<EventLite[]>(`${this.url}recent/${qty}/${page}`);
  }

  public getAllEvents(qty: number, page: number): Observable<EventLite[]> {
    return this.httpService.get<EventLite[]>(`${this.url}all/${qty}/${page}`);
  }

  public getEvent(eventId): Observable<EventDetailed> {
    return this.httpService.get<EventDetailed>(this.url + eventId);
  }

  public deleteEvent(eventId): Observable<any> {
    return this.httpService.delete(this.url + eventId);
  }

  public editEvent(event: EventModel): Observable<IdModel> {
    return this.httpService.put<IdModel>(this.url, event);
  }

  public createEvent(newEvent: EventModel): Observable<IdModel> {
    return this.httpService.post<IdModel>(this.url, newEvent);
  }
}
