import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {LocalStorageService} from './local-storage.service';
import {Observable, Subject, Subscriber} from 'rxjs';
import {AccountService} from './account.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient,
              private localStorageService: LocalStorageService,
              private accountService: AccountService) { }

  private getHeaders() {
    let authorization: string;
    const accountInfo = this.localStorageService.getAccountInfo();
    authorization = accountInfo ? 'Bearer ' + accountInfo.token : '';

    const headers = {
      Authorization: authorization
    };

    return { headers: new HttpHeaders(headers) };
  }

  public get<T>(url): Observable<T> {
    return new Observable((s: Subscriber<T>) => {
      this.http.get<T>(url, this.getHeaders())
        .subscribe(n => s.next(n), (error => {
          this.handleError(error);
          s.error(error);
        }));
    });
  }

  public post<T>(url, body): Observable<T> {
    // @ts-ignore
    return new Observable((s: Subscriber<T>) => {
      this.http.post<T>(url, body, this.getHeaders())
        .subscribe(n => s.next(n), (error => {
          this.handleError(error);
          s.error(error);
        }));
    });
  }

  public put<T>(url, body): Observable<T> {
    // @ts-ignore
    return new Observable((s: Subscriber<T>) => {
      this.http.put<T>(url, body, this.getHeaders())
        .subscribe(n => s.next(n), (error => {
          this.handleError(error);
          s.error(error);
        }));
    });
  }

  public delete<T>(url): Observable<T> {
    // @ts-ignore
    return new Observable((s: Subscriber<T>) => {
      this.http.delete<T>(url, this.getHeaders())
        .subscribe(n => s.next(n), (error => {
          this.handleError(error);
          s.error(error);
        }));
    });
  }


  public handleError(error: HttpErrorResponse) {
    const err = error.error.error;
    if (error.status === 401 && err === 'invalid_token') {
      alert('La sessione è scaduta!');
      this.accountService.logout();
    } else {
      console.log('other error');
      console.log(error);
    }
  }
}
