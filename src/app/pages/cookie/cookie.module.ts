import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookieComponent } from './cookie/cookie.component';
import {CookieRoutingModule} from './cookie-routing';
import { CookieCompleteComponent } from './cookie-complete/cookie-complete.component';
import {NgcCookieConsentModule} from 'ngx-cookieconsent';

@NgModule({
  declarations: [CookieComponent, CookieCompleteComponent],
  imports: [
    CommonModule,
    CookieRoutingModule,
    NgcCookieConsentModule
  ]
})
export class CookieModule { }
