import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CookieComponent} from './cookie/cookie.component';
import {CookieCompleteComponent} from './cookie-complete/cookie-complete.component';

const routes: Routes = [
  { path: '', component: CookieComponent },
  { path: 'complete', component: CookieCompleteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CookieRoutingModule { }
