import { Component, OnInit } from '@angular/core';
import {NgcCookieConsentService} from 'ngx-cookieconsent';

@Component({
  selector: 'app-cookie-complete',
  templateUrl: './cookie-complete.component.html',
  styleUrls: ['./cookie-complete.component.scss']
})
export class CookieCompleteComponent implements OnInit {

  constructor(private ccService: NgcCookieConsentService) { }

  ngOnInit() {
    this.ccService.destroy();
  }

}
