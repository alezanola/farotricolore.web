import { Component, OnInit } from '@angular/core';
import {NgcCookieConsentService} from 'ngx-cookieconsent';

@Component({
  selector: 'app-cookie',
  templateUrl: './cookie.component.html',
  styleUrls: ['./cookie.component.scss']
})
export class CookieComponent implements OnInit {

  constructor(private ccService: NgcCookieConsentService) { }

  ngOnInit() {
    this.ccService.destroy();
  }

}
