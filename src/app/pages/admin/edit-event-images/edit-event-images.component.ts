import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {AccountService} from '../../shared/services/account.service';
import {EventMediaService} from '../../shared/services/event-media.service';
import {EventMedia} from '../../shared/models/event-media';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Video} from '../../shared/models/video';

@Component({
  selector: 'app-edit-event-images',
  templateUrl: './edit-event-images.component.html',
  styleUrls: ['./edit-event-images.component.scss']
})
export class EditEventImagesComponent implements OnInit {
  private eventId: string;
  loading = true;
  event: EventMedia;

  uploading = false;
  private imagesToUpload: string[];

  youtubeCode: string = '';

  constructor(private route: ActivatedRoute,
              private accountService: AccountService,
              private router: Router,
              public modalService: NgbModal,
              private formBuilder: FormBuilder,
              private eventImagesService: EventMediaService,
              private eventMediaService: EventMediaService) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.eventId = params.get('id');
      this.loadData();
    });
  }

  loadData() {
    this.eventMediaService.getEventMedia(this.eventId).subscribe(evt => {
      this.event = evt;
      this.loading = false;
    });
  }

  onSubmit(files: FileList) {
    const filesArray = Array.from(files);
    this.imagesToUpload = [];
    this.uploading = true;

    this.loadBase64(filesArray);
  }

  private loadBase64(filesArray: File[]) {
    const [head, ...tail] = filesArray;
    const reader = new FileReader();

    reader.onloadend = () => {
      const base64 = reader.result.toString();
      this.imagesToUpload.push(base64);

      if (tail.length) this.loadBase64(tail);
      else this.insertImages(this.imagesToUpload);
    };
    reader.readAsDataURL(head);
  }

  private insertImages(images: string[]) {
    const [head, ...tail] = images;

    this.eventImagesService.insertImages(this.eventId, [head]).subscribe(
      () => {
        this.loadData();

        if(tail.length) this.insertImages(tail);
        else {
          this.imagesToUpload = [];
          this.uploading = false;
        }
      }, error => {
        this.uploading = false;
        this.loadData();
        alert('Errore! Si prega di riprovare.\n' + error.error.error);
      });
  }

  deleteImage(id) {
    this.eventImagesService.deleteImage(this.eventId, id).subscribe(res => {
      this.loadData();
    }, error => {
      alert('Errore! Si prega di riprovare.\n' + error.error.error);
    });
  }

  addVideo() {
    let v: Video = {
      id: '',
      youTubeCode: this.youtubeCode
    }
    this.event.videos = this.event.videos ? this.event.videos : [];
    this.event.videos.push(v);
    this.updateVideos();
  }

  deleteVideo(id: string) {
    this.event.videos = this.event.videos.filter(t => t.id != id);
    this.updateVideos();
  }

  updateVideos() {
    this.eventMediaService.editVideos(this.eventId, this.event.videos).subscribe(
      () => {
        console.log('Video aggiornati!');
      }, error => {
        alert('Errore durante il caricamento del video!');
        console.log(error);
      },
      () => {
        this.loadData();
      });
  }
}
