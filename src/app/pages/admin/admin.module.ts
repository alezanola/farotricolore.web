import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LogoutComponent } from './logout/logout.component';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { EditEventComponent } from './edit-event/edit-event.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import {UserService} from './shared/services/user.service';
import { EditEventImagesComponent } from './edit-event-images/edit-event-images.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {LoginActivate} from './shared/services/login-activate';

@NgModule({
  declarations: [
    LoginComponent,
    DashboardComponent,
    LogoutComponent,
    EditEventComponent,
    EditUserComponent,
    EditEventImagesComponent
  ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedComponentsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        CKEditorModule
    ],
  providers: [
    UserService,
    LoginActivate
  ]
})
export class AdminModule { }
