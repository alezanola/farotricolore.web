import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventModel} from '../shared/models/event-model';
import {UserService} from '../shared/services/user.service';
import {map, switchMap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserModel} from '../shared/models/user-model';
import {CommonService} from '../../shared/services/common.service';
import {AccountService} from '../../shared/services/account.service';
import {AppConfig} from '../../../shared/config/app-config';

@Component({
  selector: 'app-create-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  form: FormGroup;
  loading = false;
  editing = false;
  editingUserId: string;
  roles = [
    { name: 'Admin', value: 'Admin' },
    { name: 'Gestore eventi', value: 'EventManager' }
  ];

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private accountService: AccountService,
              private router: Router,
              private appConfig: AppConfig) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([ Validators.required, Validators.email ])],
      password: ['', Validators.compose([ Validators.required, Validators.minLength(this.appConfig.passwordMinLength) ])],
      role: ['', Validators.compose([ Validators.required ])],
      name: ['', Validators.compose([ Validators.required ])],
      surname: ['', Validators.compose([ Validators.required ])]
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      if (params.has('id')) {
        this.editingUserId = params.get('id');
        this.loadData(this.editingUserId);
      }
    });
  }

  private loadData(userId) {
    this.editing = true;
    this.disableForm();
    this.userService.getUser(userId).subscribe(user => {
      this.form.setValue({
        email: user.email,
        password: user.password,
        role: user.role,
        name: user.name,
        surname: user.surname
      });
      this.form.setControl('password', this.formBuilder.control('', Validators.minLength(this.appConfig.passwordMinLength)));
      this.enableForm();
    });
  }

  onSubmit(userData) {
    this.disableForm();
    const user: UserModel = {
      id: this.editing ? this.editingUserId : '',
      email: userData.email,
      password: userData.password,
      role: userData.role,
      name: userData.name,
      surname: userData.surname
    };

    const errorCallback = error => {
      alert(error.error.error);
      this.enableForm();
    };

    if (this.editing) {
      this.userService.editUser(user)
        .subscribe(eventId => {
            this.router.navigateByUrl('/admin');
          },
          errorCallback);
    } else {
      this.userService.createUser(user)
        .subscribe(eventId => {
            this.router.navigateByUrl('/admin');
          },
          errorCallback);
    }
  }

  private disableForm() {
    this.loading = true;
    this.form.disable();
  }

  private enableForm() {
    this.loading = false;
    this.form.enable();
  }
}
