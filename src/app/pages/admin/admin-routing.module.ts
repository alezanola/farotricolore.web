import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './logout/logout.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {EditEventImagesComponent} from './edit-event-images/edit-event-images.component';
import {LoginActivate} from './shared/services/login-activate';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [LoginActivate] },
  { path: 'event', component: EditEventComponent, canActivate: [LoginActivate] },
  { path: 'event/:id', component: EditEventComponent, canActivate: [LoginActivate] },
  { path: 'event/:id/images', component: EditEventImagesComponent, canActivate: [LoginActivate] },
  { path: 'user', component: EditUserComponent, canActivate: [LoginActivate] },
  { path: 'user/:id', component: EditUserComponent, canActivate: [LoginActivate] },
  { path: 'logout', component: LogoutComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
