import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import {AppConfig} from '../../../shared/config/app-config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  protected loading: boolean;

  constructor(private usersService: AccountService,
              private localStorageService: LocalStorageService,
              private router: Router,
              private formBuilder: FormBuilder,
              private appConfig: AppConfig) {

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([ Validators.required, Validators.email ])],
      password: ['', Validators.compose([ Validators.required, Validators.minLength(appConfig.passwordMinLength) ])]
    });
    // this.loginForm.
  }

  ngOnInit() {
    if (this.usersService.isLoggedIn()) {
      this.router.navigateByUrl('/admin');
    }
    this.loading = false;
  }

  onSubmit(loginData) {
    this.loading = true;
    this.loginForm.disable();
    this.usersService.authenticate(loginData)
      .subscribe(accountInfo => {
        this.localStorageService.setAccountInfo(accountInfo);
        this.router.navigateByUrl('/admin');
      },
      error => {
        alert(error.error.error);
        this.loading = false;
        this.loginForm.enable();
      });
  }

  handleError(httpError: HttpErrorResponse) {
    console.log(httpError);
  }
}
