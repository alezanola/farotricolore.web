import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { Router } from '@angular/router';
import { EventLite } from '../../shared/models/event-lite';
import { EventsService } from '../../shared/services/events.service';
import {CommonService} from '../../shared/services/common.service';
import {UserService} from '../shared/services/user.service';
import {UserModel} from '../shared/models/user-model';
import {LocalStorageService} from '../../shared/services/local-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  events: EventLite[];
  users: UserModel[];
  eventsLoading = true;
  usersLoading = true;
  showUsers = false;
  public loadingMoreEvents: boolean;
  public qty: number;
  public page: number;


  constructor(private accountService: AccountService,
              private router: Router,
              private eventsService: EventsService,
              private userService: UserService,
              public commonService: CommonService,
              private localStorageService: LocalStorageService) {}

  ngOnInit() {
    this.loadingMoreEvents = false;
    this.qty = 8;
    this.page = 0;
    this.loadEvents();
    this.showUsers = this.localStorageService.getAccountInfo().role === 'Admin';
    if (this.showUsers) {
      this.loadUsers();
    }
  }

  loadEvents() {
    this.eventsService.getAllEvents(this.qty, this.page).subscribe(e => {
      this.events = e;
      this.eventsLoading = false;
    });
  }

  loadMoreEvents() {
    this.page++;
    this.loadingMoreEvents = true;
    this.eventsService.getAllEvents(this.qty, this.page).subscribe(events => {
      this.events = this.events.concat(events);
      this.loadingMoreEvents = false;
    });
  }

  loadUsers() {
    this.userService.getUsers().subscribe(u => {
      this.users = u;
      this.usersLoading = false;
    });
  }

  deleteEvent(id: string) {
    this.eventsService.deleteEvent(id).subscribe((response) => {
      this.loadEvents();
    });
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe((response) => {
      this.loadUsers();
    });
  }

  confirmMessage(message: string) {
    return confirm(message);
  }
}
