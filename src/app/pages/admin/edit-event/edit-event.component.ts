import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventsService } from '../../shared/services/events.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import {EventModel} from '../shared/models/event-model';
import {AccountService} from '../../shared/services/account.service';
import {Platform} from '@angular/cdk/platform';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

  form: FormGroup;
  public descriptionEditor;
  loading = false;
  editing = false;
  startedEditing = false;
  editingEventId: string;
  editingHideEvent: boolean;
  editingPosterUrl: string;
  posterBase64: string;
  removePoster: boolean = false;

  constructor(private eventsService: EventsService,
              private localStorageService: LocalStorageService,
              private accountService: AccountService,
              public router: Router,
              public route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private platform: Platform) {}

  ngOnInit() {
    // For Server Side Rendering
    if (this.platform.isBrowser) {
      this.loadCkEditor();
    }

    this.form = this.formBuilder.group({
      name: ['', Validators.compose([ Validators.required ])],
      location: ['', Validators.compose([ Validators.required ])],
      startDate: ['', Validators.compose([ Validators.required ])],
      endDate: ['', Validators.compose([])],
      shortDescription: ['', Validators.compose([ Validators.required ])],
      longDescription: ['', Validators.compose([ Validators.required ])],
      poster: ['', Validators.compose([])]
    });

    this.form.disable();

    this.route.paramMap.subscribe((params: ParamMap) => {
      if (params.has('id')) {
        this.editingEventId = params.get('id');
        this.loadData(this.editingEventId);
      } else {
        this.enableForm();
      }
    });
  }

  private enableForm() {
    this.form.enable();
    this.form.valueChanges.subscribe(value => {
      this.startedEditing = true;
    })
  }

  private loadData(eventId) {
    this.editing = true;
    this.eventsService.getEvent(eventId).subscribe(event => {
      this.form.setValue({
        name: event.name,
        location: event.location,
        startDate: event.startDate,
        endDate: event.endDate,
        shortDescription: event.shortDescription,
        longDescription: event.longDescription,
        poster: ''
      });
      this.editingHideEvent = false;
      this.editingPosterUrl = event.poster ? event.poster.large : '';

      this.enableForm();
    });
  }

  showInfo(message: string) {
    alert(message);
  }

  onSubmit(eventData) {
    // if (!this.editing && !this.editingHideEvent && (this.posterBase64 == null)) {
    //   alert('Immagine non caricata. Riprovare!');
    //   return;
    // }
    this.loading = true;
    this.form.disable();
    const eventToCreate: EventModel = {
      id: this.editing ? this.editingEventId : '',
      name: eventData.name,
      location: eventData.location,
      startDate: eventData.startDate,
      endDate: eventData.endDate ? eventData.endDate : eventData.startDate,
      shortDescription: eventData.shortDescription,
      longDescription: eventData.longDescription,
      posterBase64: this.posterBase64,
      hideEvent: this.editingHideEvent,
      removePoster: this.removePoster
    };

    const errorCallback = error => {
      alert(error.error.error);
    };

    if (this.editing) {
      this.eventsService.editEvent(eventToCreate)
        .subscribe(() => {
            alert(this.editingHideEvent ? 'Bozza salvata' : 'Evento pubblicato');
            this.editingHideEvent = false;
            this.loading = false;
            this.form.enable();
            this.startedEditing = false;
          },
          errorCallback);
    } else {
      this.eventsService.createEvent(eventToCreate)
        .subscribe(eventId => {
            this.router.navigateByUrl('/admin');
            this.loading = false;
            this.editingHideEvent = false;
            this.form.enable();
          },
          errorCallback);
    }
  }

  onFileUploadChange(file) {
    this.removePoster = false;
    const reader = new FileReader();

    reader.onloadend = () => {
      this.posterBase64 = reader.result.toString();
    };
    reader.readAsDataURL(file);
  }

  confirmLoseEditsMessage(message: string) {
    return !this.startedEditing || confirm(message);
  }

  removePosterClick(fileInput) {
    if (confirm('Rimuovere veramente la locandina dall\'evento?')) {
      fileInput.value = ''

      this.posterBase64 = '';
      this.editingPosterUrl = '';
      this.startedEditing = true;
      this.removePoster = true;
    }
  }


  // For Server Side Rendering
  loadCkEditor() {
    let editorUrl = 'https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js';
    let loaded = false;

    // Check if already loaded
    const scripts = document.getElementsByTagName('script')
    for(let i = 0; i < scripts.length; i++){
      if (scripts[i].src === editorUrl){
        this.descriptionEditor = (window as any).ClassicEditor;
        loaded = true;
        break;
      }
    }

    // Load if not already loaded
    if(!loaded){
      const jsElmCK = document.createElement('script');
      jsElmCK.type = 'application/javascript';
      jsElmCK.src = editorUrl;
      document.body.appendChild(jsElmCK);
      jsElmCK.onload = () => {
        this.descriptionEditor = (window as any).ClassicEditor;
      };
    }
  }
}
