import { Injectable } from '@angular/core';
import {AppConfig} from '../../../../shared/config/app-config';
import {LocalStorageService} from '../../../shared/services/local-storage.service';
import {Router} from '@angular/router';
import {HttpService} from '../../../shared/services/http.service';
import {UserModel} from '../models/user-model';
import {Observable} from 'rxjs';
import {IdModel} from '../../../shared/models/id-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly url: string;

  constructor(private httpService: HttpService,
              private config: AppConfig) {
    this.url = config.baseV1Url + 'user/';
  }

  public getUsers(): Observable<UserModel[]> {
    return this.httpService.get<UserModel[]>(this.url);
  }

  public getUser(userId: string): Observable<UserModel> {
    return this.httpService.get<UserModel>(this.url + userId);
  }

  public createUser(user: UserModel): Observable<IdModel> {
    return this.httpService.post<IdModel>(this.url, user);
  }

  public editUser(user: UserModel): Observable<IdModel> {
    return this.httpService.put<IdModel>(this.url, user);
  }

  public deleteUser(userId: string): Observable<any> {
    return this.httpService.delete(this.url + userId);
  }
}
