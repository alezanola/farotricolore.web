import { Injectable } from '@angular/core';
import {AccountService} from '../../../shared/services/account.service';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable()
export class LoginActivate {
  constructor(private accountService: AccountService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    if (!this.accountService.isLoggedIn()) {
      this.router.navigate(['/admin/login']);
    }
    return true;
  }
}
