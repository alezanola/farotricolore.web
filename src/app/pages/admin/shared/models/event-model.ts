export class EventModel {
  id: string;
  name: string;
  shortDescription: string;
  longDescription: string;
  startDate: Date;
  endDate: Date;
  posterBase64: string;
  location: string;
  hideEvent: boolean;
  removePoster: boolean;
}
