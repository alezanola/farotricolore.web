import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';

@Component({
  template: ''
})
export class LogoutComponent implements OnInit {

  constructor(private userService: AccountService) { }

  ngOnInit() {
    this.userService.logout();
  }

}
