import {Component, OnInit} from '@angular/core';
import {EventWithFirstImage} from '../../shared/models/event-with-first-image';
import {EventMediaService} from '../../shared/services/event-media.service';
import {CommonService} from '../../shared/services/common.service';
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  events: EventWithFirstImage[];
  loading: boolean;
  public loadingMoreEvents: boolean;
  public qty: number;
  public page: number;


  constructor(private eventImagesService: EventMediaService,
              public commonService: CommonService,
              public router: Router,
              private titleService: Title,
              private metaService: Meta) {
    this.setTitle();
    this.configureMeta();
  }

  ngOnInit() {
    this.loading = true;
    this.loadingMoreEvents = false;
    this.qty = 10;
    this.page = 0;

    this.eventImagesService.getPastEventsImage(this.qty, this.page).subscribe(events => {
      this.events = events;
      this.loading = false;
      if(events.length < 5)
        this.loadMoreEvents()
    });
  }

  loadMoreEvents() {
    this.page++;
    this.loadingMoreEvents = true;
    this.eventImagesService.getPastEventsImage(this.qty, this.page).subscribe(events => {
      this.events = this.events.concat(events);
      this.loadingMoreEvents = false;
    });
  }

  setTitle(): void {
    this.titleService.setTitle('Galleria - Faro Tricolore');
  }

  configureMeta(): void {
    this.metaService.addTags([
      {
        name: 'og:description',
        content: "Galleria delle foto e dei video dei nostri eventi."
      },
      {
        name: 'og:image',
        content: '/assets/images/logos/LogoFaroB.png'
      },
      { name: 'robots', content: 'index, follow' }
    ]);
  }
}
