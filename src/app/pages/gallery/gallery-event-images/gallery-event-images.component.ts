import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent} from 'ngx-image-gallery';
import {GalleryImage} from '../../shared/models/gallery-image';
import {GalleryConf} from '../../shared/models/gallery-conf';
import {Location} from '@angular/common';
import {EventMedia} from '../../shared/models/event-media';
import {EventMediaService} from '../../shared/services/event-media.service';

@Component({
  selector: 'app-gallery-event-images',
  templateUrl: './gallery-event-images.component.html',
  styleUrls: ['./gallery-event-images.component.scss']
})
export class GalleryEventImagesComponent implements OnInit {
  loading = true;
  event: EventMedia;

  @ViewChild('imageGallery', { static: true }) galleryComponent: NgxImageGalleryComponent;
  galleryImages: GALLERY_IMAGE[];
  galleryConf: GALLERY_CONF;

  constructor(private eventMediaService: EventMediaService,
              private route: ActivatedRoute,
              public location: Location) {
    this.galleryConf = new GalleryConf();
    // this.galleryConf.showDeleteControl = false;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(param => {
      const id = param.get('id');
      this.loadEvent(id);
    });
  }

  loadEvent(id) {
    // if (this.event && this.event.images) {
    //   this.galleryImages = this.event.images.map(img => {
    //     const galleryImage = new GalleryImage();
    //     galleryImage.url = img.large;
    //     return galleryImage;
    //   });
    // }
    this.eventMediaService.getEventMedia(id).subscribe(evt => {
      this.event = evt;
      if(this.event.images) {
        this.galleryImages = this.event.images.map(img => {
          const galleryImage = new GalleryImage();
          galleryImage.url = img.large;
          return galleryImage;
        });
      }
      this.loading = false;
    });
  }

}
