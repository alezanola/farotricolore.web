import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GalleryComponent} from './gallery/gallery.component';
import {GalleryEventImagesComponent} from './gallery-event-images/gallery-event-images.component';

const routes: Routes = [
  { path: '', component: GalleryComponent },
  { path: ':id', component: GalleryEventImagesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryRoutingModule { }
