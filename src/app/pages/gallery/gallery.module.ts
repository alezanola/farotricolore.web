import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GalleryComponent} from './gallery/gallery.component';
import {GalleryEventImagesComponent} from './gallery-event-images/gallery-event-images.component';
import {SharedComponentsModule} from '../shared/components/shared-components.module';
import {GalleryRoutingModule} from './gallery-routing';
import {NgxImageGalleryModule} from 'ngx-image-gallery';



@NgModule({
  declarations: [
    GalleryComponent,
    GalleryEventImagesComponent
  ],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    SharedComponentsModule,
    NgxImageGalleryModule
  ]
})
export class GalleryModule { }
