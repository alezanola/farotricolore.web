import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppConfig } from './config/app-config';

@NgModule({
  declarations: [],
  providers: [
    AppConfig
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class SharedModule { }
