export class AppConfig {
  private baseUrl: string;

  public baseV1Url: string;

  public passwordMinLength: number;

  constructor() {
    // this.baseUrl = 'https://farotricolore-api.azurewebsites.net/';
    this.baseUrl = 'https://farotricolore-api-jccedtyu3q-ew.a.run.app/';
    // this.baseUrl = 'http://localhost:5005/';
    // this.baseUrl = 'http://192.168.10.131:5005/';

    this.baseV1Url = this.baseUrl + 'v1/';

    this.passwordMinLength = 5;
  }
}
