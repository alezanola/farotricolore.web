import { Component, OnInit, HostListener } from '@angular/core';
import { AccountService } from '../pages/shared/services/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {
  navItems: { name: string, link: string, exactPath: boolean, children?: { name: string, link?: string }[] }[] = [
    { name: 'Home', link: '/', exactPath: true },
    { name: 'Chi siamo', link: '/chisiamo', exactPath: false},
    { name: 'Eventi', link: '/events', exactPath: false },
    { name: 'Galleria', link: '/gallery', exactPath: false },
    // { name: 'Contatti', link: '/contacts', exactPath: false },
  ];

  navItemsAdmin: { name: string, link: string, exactPath: boolean, children?: { name: string, link?: string }[] }[] = [
    { name: 'Dashboard', link: '/admin/dashboard', exactPath: false },
    // { name: 'Account', link: '/admin/account', exactPath: false },
    { name: 'Logout', link: '/admin/logout', exactPath: false },
  ];

  showNavItemsAdmin = false;
  isNavCollapsed = true;
  isMobile = false;

  constructor(private userService: AccountService,
              private router: Router) {
    this.onResize();
  }

  ngOnInit() {
    this.showNavItemsAdmin = this.userService.isLoggedIn();
    this.userService.subscribeLoginState(state => {
      this.showNavItemsAdmin = state;
    });
  }

  navLinkClick(url: string) {
    this.isNavCollapsed = true;
    this.router.navigateByUrl(url);
  }

  isNavLinkActive(url) {
    return url === this.router.url;
  }

  isAdminPage() {
    return this.router.url.startsWith('/admin');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    if(this.isMobile != window.innerWidth <= 575)
      this.isMobile = !this.isMobile;
  }
}
