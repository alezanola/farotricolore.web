FROM node:12 AS build

# Create src directory
WORKDIR /src

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
# Bundle app source
COPY . .

RUN npm install
RUN npm run build:ssr

# For runtime
FROM node:12-slim AS runtime
WORKDIR /app
COPY --from=build /src/dist .
CMD [ "node", "server" ]
